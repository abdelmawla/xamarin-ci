module BuildHelpers

#r "System.Core.dll"
#r "System.Xml.Linq.dll"

open Fake
open Fake.XamarinHelper
open System
open System.IO
open System.Linq
open Fake.XMLHelper
open System.Xml.Linq

let Exec command args =
    let result = Shell.Exec(command, args)

    if result <> 0 then failwithf "%s exited with error %d" command result

let RestorePackages solutionFile =
    Exec "tools/NuGet/NuGet.exe" ("restore " + solutionFile)
    solutionFile |> RestoreComponents (fun defaults -> {defaults with ToolPath = "tools/xpkg/xamarin-component.exe" })

let GetBuildCounter buildNumber =
    let versionPrefix = "1.0"
    let versionSuffix = "0"
    if String.IsNullOrEmpty(buildNumber) then failwithf "%s" "build number needed"
    versionPrefix + "."+ buildNumber  + "."+ versionSuffix

let UpdatePlist version project appTitle bundleId =
    let info = Path.Combine(project, "Info.plist")
    // updating version number
    Exec "/usr/libexec/PlistBuddy" ("-c 'Set :CFBundleShortVersionString " + version + "' " + info)
    Exec "/usr/libexec/PlistBuddy" ("-c 'Set :CFBundleVersion " + version + "' " + info)
    // updating bundle id
    Exec "/usr/libexec/PlistBuddy" ("-c 'Set :CFBundleIdentifier " + bundleId + "' " + info)
    Exec "/usr/libexec/PlistBuddy" ("-c 'Set :CFBundleDisplayName " + appTitle + "' " + info)

let RenamePackageFile packageFile newName =
    let fi = new FileInfo(packageFile)
    let newFileName = Path.Combine(fi.Directory.FullName, newName)
    File.Copy(packageFile, newFileName)
    newFileName

let UpdateManifest version project appTitle bundleId buildNumber =
    let path = (project + "/Properties/AndroidManifest.xml")
    let ns = Seq.singleton(("android", "http://schemas.android.com/apk/res/android"))
    XmlPokeNS path ns "manifest/@android:versionName" (version)
    XmlPokeNS path ns "manifest/@android:versionCode" buildNumber
    XmlPokeNS path ns "manifest/application/@android:label" appTitle
    XmlPokeNS path ns "manifest/@package" bundleId

let UpdateBuildConfiguration env =
    let path = Path.Combine("Xamarin.Shared", "Configuration", "config.xml")
    if String.IsNullOrEmpty(env) then failwithf "%s" "please, make sure current environment flag is already set"
    let reader = new StreamReader(File.OpenRead(path))
    let doc =  XDocument.Load(reader)
    reader.Close()
    let configSection = doc.Root
    let envSection = configSection.Descendants(XName.Get "Env").FirstOrDefault()
    envSection.Value <- (string)env
    doc.Save(path)
