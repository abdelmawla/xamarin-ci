﻿using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace Xamarin.Shared.Configuration
{
    public class ConfigReader
    {
        private const string ConfigFileName = "Xamarin.Shared.Configuration.config.xml";
        private Config _configSection;


        public ConfigReader()
        {
           _configSection =  LoadConfiSections();
        }

        private Config LoadConfiSections()
        {
            try
            {
                using (Stream stream = GetType().GetTypeInfo().Assembly.GetManifestResourceStream(ConfigFileName))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        var serializer = new XmlSerializer(typeof (Config));
                        return _configSection = (Config) serializer.Deserialize(reader);
                    }
                }
            }
            catch(Exception exception)
            {
                return new Config();
            }
        }

        public Config ActiveConfig
        {
            get { return _configSection; }
        }
    }
}
