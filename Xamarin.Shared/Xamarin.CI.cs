﻿using Xamarin.Forms;
using Xamarin.Shared.Configuration;

namespace Xamarin.Shared
{
	public class App : Application
	{
		public App ()
		{

		    var activeConfig = new ConfigReader().ActiveConfig;

			// The root page of your application
			MainPage = new ContentPage {
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
					Children = {
						new Label {
							XAlign = TextAlignment.Center,
							Text = string.Format("Hello {0} Enviroment", activeConfig.Env) 
						}
					}
				}
			};
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

