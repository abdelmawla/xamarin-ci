#light
#r "packages/FAKE/tools/FakeLib.dll"
#load "build-helpers.fsx"

open Fake
open System
open System.IO
open System.Linq
open BuildHelpers
open Fake.XamarinHelper
open Fake.HockeyAppHelper
open Fake.EnvironmentHelper

//Configuration
let iOSBinDir = "./iOS/bin/"
let iOSObjDir = "./iOS/obj/"
let androidBinDir = "./Droid/bin/"
let androidObjDir = "./Droid/obj/"
let xamarinSharedBinDir = "./Xamarin.Shared/bin"
let xamarinSharedObjDir = "./Xamarin.Shared/obj"
let packagesFolder = "./packages/"

let Env = Environment.GetEnvironmentVariable "Env"
let Bundle = Environment.GetEnvironmentVariable "Bundle"
let AppName = Environment.GetEnvironmentVariable "AppName"
let BuildConfig = Environment.GetEnvironmentVariable "BuildConfig"
let ReleaseType = Environment.GetEnvironmentVariable "ReleaseType"
let SolutionPath =  "Xamarin.sln"
let DroidOutput =  "output/Android/"
let KeystorePath = Environment.GetEnvironmentVariable "KeystorePath"
let KeystorePassword = Environment.GetEnvironmentVariable "KeystorePassword"
let KeystoreAlias = Environment.GetEnvironmentVariable "KeystoreAlias"
let HockeyApiToken =   Environment.GetEnvironmentVariable "HockeyApiToken"
let BuildNumber = Environment.GetEnvironmentVariable "BuildNumber"

let GetIOSOutputFolder envFlag =
    match envFlag with
      | "Prod" -> Path.Combine("iOS", "bin", "iPhone", "Release")
      | "Staging" -> Path.Combine("iOS", "bin", "iPhone", "AdHoc")
      | "" -> Path.Combine("iOS", "bin", "iPhone", "AdHoc")

Target "Clean" (fun _ ->
    CleanDirs [iOSBinDir; iOSObjDir; androidBinDir; androidObjDir; DroidOutput; xamarinSharedBinDir; xamarinSharedObjDir]
)

Target "ios-build" (fun () ->
  BuildHelpers.UpdateBuildConfiguration Env

  RestorePackages SolutionPath

  let buildCounter = (BuildHelpers.GetBuildCounter BuildNumber)

  UpdatePlist buildCounter "iOS" AppName Bundle

  iOSArchive(fun defaults ->
    {defaults with
        ProjectName = "XamariniOS"
        Configuration = BuildConfig
        SolutionPath = SolutionPath
    })
)

Target "droid-build" (fun () ->
  BuildHelpers.UpdateBuildConfiguration Env

  RestorePackages SolutionPath

  let buildCounter = (BuildHelpers.GetBuildCounter BuildNumber)

  UpdateManifest buildCounter "Droid" AppName Bundle BuildNumber

  AndroidPackage (fun defaults ->
      {defaults with
          ProjectPath = "Droid/Xamarin.Droid.csproj"
          Configuration = BuildConfig
          OutputPath = DroidOutput
      })|>
     AndroidSignAndAlign (fun defaults ->
        {defaults with
            KeystorePath = KeystorePath
            KeystorePassword = KeystorePassword
            KeystoreAlias = KeystoreAlias
        })|> ignore
)

Target "ios-deploy" (fun () ->
    let appPath = Directory.EnumerateFiles((GetIOSOutputFolder Env), "*.ipa").First()

    let buildCounter = (BuildHelpers.GetBuildCounter BuildNumber)

    let newPackageName = String.Format("{0}.{1}.{2}.ipa", AppName, Env, buildCounter)

    let newCreatedPackage = BuildHelpers.RenamePackageFile appPath newPackageName

    HockeyApp(fun defaults ->
      {defaults with
          ApiToken = HockeyApiToken
          File = newCreatedPackage
          Notes = "Build # " + buildCounter
          Notify = NotifyOption.All
          ReleaseType = Fake.HockeyAppHelper.ReleaseType.Beta
      })
    |> ignore
)

Target "droid-deploy" (fun () ->
    let buildCounter = (BuildHelpers.GetBuildCounter BuildNumber)

    let newPackageName = String.Format("{0}.{1}.{2}.apk", AppName, Env, buildCounter)

    let oldPackageFile = Directory.EnumerateFiles(DroidOutput, "*.apk", SearchOption.AllDirectories).Where(fun file -> file.Contains("Aligned")).First()

    let newCreatedPackage = BuildHelpers.RenamePackageFile oldPackageFile newPackageName

    HockeyApp(fun defaults ->
      {defaults with
          ApiToken = HockeyApiToken
          File = newCreatedPackage
          Notes = "Build # " + buildCounter
          Notify = NotifyOption.All
          ReleaseType = Fake.HockeyAppHelper.ReleaseType.Beta
      })
    |> ignore
)

"Clean"
    ==> "ios-build"

"Clean"
    ==> "droid-build"

RunTargetOrDefault "Clean"
